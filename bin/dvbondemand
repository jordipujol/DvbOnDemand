#!/bin/bash

#  dvbondemand
#
#  Gets audio/video services from several dvb cards.
#  Starts mumudvb servers on client's request.
#  Stops these servers when idle.
#
#  dvbondemand detects when a service is required and therefore
#  starts mumudvb sessions using an available dvb adapter.
#  In few seconds clients will get the corresponding http video/audio stream.
#  Also, ends the mumudvb server session after client disconnection.
#
#  $Revision: 1.36 $
#
#  Copyright (C) 2023-2024 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

set -o errexit -o nounset -o pipefail +o noglob +o noclobber +o monitor
shopt -s extglob

_trim() {
	printf '%s\n' "${@}" | \
	sed -re "/^[[:blank:]]+|[[:blank:]]+$/s///g"
}

_RcInt() {
	kill -l "${@}" | \
		awk '{printf "%d\t", $1+128}'
}

_lsof() {
	local f="${1}" \
		inum
	inum="$(ls -i "$(readlink -f "${f}")" 2>> "${devNull}" )" && \
	( find /proc/+([0-9])/fd -follow -inum ${inum%% *} 2> /dev/null || : ) | \
		cut -f 3 -s -d '/' | \
		sort -n -u | \
		grep -sxvF "1"
}

_ps_children() {
	local ppid=${1:-${$}} \
		excl="${2:-"0"}" \
		pid
	for pid in $(pgrep -P ${ppid} | \
	grep -svwEe "${excl}"); do
		_ps_children ${pid} "${excl}"
		printf '%d\t' ${pid}
	done
}

_pids_active() {
	local p rc=${ERR}
	for p in "${@}"; do
		if kill -s 0 ${p} 2> /dev/null; then
			printf '%d\t' ${p}
			rc=${OK}
		fi
	done
	return ${rc}
}

_wait() {
	local pids="${@:-"$(pgrep -P ${$})"}"
	while wait ${pids} 2>> "${devNull}" || : ;
	pids="$(_pids_active ${pids})"; do
		:
	done
}

_UTCseconds() {
	date +'%s' "${@}"
}

_datetime() {
	date +'%F %T' "${@}"
}

_applog() {
	local msg="${@}"
	printf '%s\n' "$(_datetime) ${msg}" >> "${LOG}"
}

LogMsg() {
	local event="${1}" \
		clientIPs="${2}" \
		msg
	shift; shift
	msg="${@}"
	_applog "${msg}"
	[ -z "${ExitPoints}" ] || \
		(("${event}" "${msg}" ${clientIPs} >> "${devNull}" 2>&1) &)
}

PreBackupRotate() {
	local f d=$(_UTCseconds)
	for f in "${LOG}" "${LOG}.xtrace"; do
		[ ! -f "${f}" ] || \
			mv -f "${f}" "${f}_${d}"
	done
}

BackupRotate() {
	local f t
	find . -maxdepth 1 \
	-name "${LOG}_*" | \
	sort | \
	head -qn -${LogRotate} | \
	while IFS="_" read -r f t; do
		rm -f "${LOG}_${t}" "${LOG}.xtrace_${t}"
	done
}

Traffic() {
	local portHttp=${1}
	ss --no-header --numeric --tcp state established sport ${portHttp} | \
		awk '{print $NF; rc=-1}
		END{exit rc+1}' | \
		sort
}

RuleInsert() {
	local portHttp="${1}" \
		config conffile
	[ -z "${Debug}" ] || \
		! Traffic ${portHttp} >> "${devNull}" || \
			_applog "Warn: will set watch on port ${portHttp}" \
				"when server is already active"
	iptables -4 --wait -I INPUT -p tcp \
		-m tcp --dport ${portHttp} \
		-m state --state NEW \
		-m limit --limit 20/min \
		-j LOG --log-level info --log-prefix "${NEWREQUEST}" 2>> "${devNull}"
	let "config=portHttp-PortHttp,1"
	eval conffile=\"\${config${config}_conffile:-}\"
	_applog "Info: watching port ${portHttp} for \"${conffile}\""
}

RuleClean() {
	local portHttp="${1:-}" \
		prefix="${2:-"${NEWREQUEST}"}" \
		rule n port d="y"
	port="${portHttp}"
	until [ -z "${d}" ]; do
		d=""
		n=1
		while rule="$(iptables -4 --wait --numeric --list INPUT ${n} 2>> "${devNull}")" && \
		[ -n "${rule}" ]; do
			if grep -qse \
			"LOG.*tcp dpt:${portHttp:-".*"} .*prefix \"${prefix}\"" \
			<<< "${rule}"; then
				iptables -4 --wait --delete INPUT ${n} 2>> "${devNull}"
				[ -n "${portHttp}" ] || \
					port="$(sed -nre "/.*tcp dpt:([[:digit:]]+) .*/s//\1/p" \
						<<< "${rule}")"
				_applog "Info: removing watch \"${prefix}\" on port ${port}"
				d="y"
			else
				let "n++,1"
			fi
		done
	done
}

RuleInsertClient() {
	local portHttp="${1}"
	iptables -4 --wait -I INPUT -p tcp \
		-m tcp --dport ${portHttp} \
		--tcp-flags FIN FIN \
		-j LOG --log-level info --log-prefix "${CLIENTCONNECT}${portHttp}" \
		2>> "${devNull}"
	iptables -4 --wait -I INPUT -p tcp \
		-m tcp --dport ${portHttp} \
		--tcp-flags RST RST \
		-j LOG --log-level info --log-prefix "${CLIENTCONNECT}${portHttp}" \
		2>> "${devNull}"
	iptables -4 --wait -I INPUT -p tcp \
		-m tcp --dport ${portHttp} \
		-m state --state NEW \
		-j LOG --log-level info --log-prefix "${CLIENTCONNECT}${portHttp}" \
		2>> "${devNull}"
	_applog "Info: watch client connection port ${portHttp}"
}

Hostnames() {
	local host name s=""
	for host in "${@}"; do
		if name="$(getent hosts "${host%%:*}")"; then
			name="${name##* }"
			name="${name//.$(hostname -d)}"
			[ -z "$(cut -f 2 -s -d ':' <<< "${host}")" ] || \
				name="${name}:${host##*:}"
		else
			name="${host}"
		fi
		printf '%s%s' "${s}" "${name}"
		s=" "
	done
}

ClientIPs() {
	printf '%s\n' "${@}" | cut -f 1 -d ':' | sort -u
}

ListStatus() {
	exec > "${STAT}"
	set | sed -nre "/\(\)[[:blank:]]*$/d" \
		-e '/^[[:upper:]][[:lower:]]/p'
	for adapter in $(seq ${Adapters}); do
		echo
		set | sed -n -e "/^adapter${adapter}_/p"
	done
	for config in $(seq ${Configs}); do
		echo
		set | sed -n -e "/^config${config}_/p"
	done
}

# config variables, default values
ConfigDefault() {
	Workdir="/run/${NAME}/"
	Debug=""
	PortHttp=4000
	LogRotate=3
	IdleTimeout=60
	Blacklist=""
	BlacklistMux=""
	CanBlacklistMux=""
	[ ! -s "/etc/default/${NAME}" ] || \
		. "/etc/default/${NAME}"
	[ "${Workdir: -1}" = '/' ] || \
		Workdir="${Workdir}/"
	mkdir -p "${Workdir}"
	cd "${Workdir}"
}

ConfigLoad() {
	local name data conffile line ind \
		card tuner dvbtype item service_id adapterTypes \
		msg="Loading configuration"

	mkdir -p -m 0755 "/run/mumudvb"
	chown -R _mumudvb:video "/run/mumudvb"

	reloadConfig="y"

	ConfigDefault

	[ -z "${pidJournal:-}" ] || {
		kill -s TERM -- -${pidJournal}
		_wait ${pidJournal}
		exec {fdPipe}<&-
		rm -f "${PIPE}"
	}

	[ ! -s "/etc/default/${NAME}" ] || \
		. "/etc/default/${NAME}"

	rm -f *.{conf,m3u,log} {0..9}* "${STAT}" "${PIPE}" 2> /dev/null

	mkfifo -m a=rw "${PIPE}"
	exec {fdPipe}<> "${PIPE}"

	set -o monitor
	( set +o errexit
	while :; do
		journalctl --since="@$(_UTCseconds)" \
		--follow "--grep=${NEWREQUEST}" --output=cat >&${fdPipe}
	done) &
	pidJournal=${!}
	set +o monitor

	adapterTypes=""
	Adapters=${NONE}
	Configs=${NONE}
	PreBackupRotate
	exec >> "${LOG}" 2>&1
	_applog "${msg}"

	! printf '%s\n' "${@}" | grep -qsxiF 'debug' || \
		Debug="y"
	! printf '%s\n' "${@}" | grep -qsxiF 'xtrace' || \
		Debug="xtrace"
	set +o xtrace
	case "${Debug}" in
	"")
		devNull="/dev/null"
		;;
	xtrace)
		export PS4='+\t ${LINENO}:${FUNCNAME:+"${FUNCNAME}:"} '
		exec {BASH_XTRACEFD}>> "${LOG}.xtrace"
		set -o xtrace
		devNull="${LOG}.xtrace"
		;;
	*)
		devNull="${LOG}"
		;;
	esac

	BackupRotate
	RuleClean
	RuleClean "" "${CLIENTCONNECT}.*"

	unset $(set | awk -F '=' \
		'$1 ~ /^(adapter|config)[[:digit:]]*_/ {print $1}') 2>> "${devNull}" || :
	ind=0
	while read -r line; do
		case "${ind}" in
		0)
			data="$(sed -nr \
			-e '/Info:[ ]+DVB:[ =]+Card ([[:digit:]]+) - Tuner ([[:digit:]]+) [=]+/{
			s//\1 \2/;p;q}' \
			-e '${q1}' <<< "${line}")" || \
				continue
			let "adapter=Adapters+1,1"
			card=${data%% *}
			tuner=${data##* }
			eval adapter${adapter}_card='${card}'
			eval adapter${adapter}_tuner='${tuner}'
			[ ${tuner} -eq 0 ] || \
				for item in demux dvr net; do
					[ -e "/dev/dvb/adapter${card}/${item}${tuner}" ] || \
						ln -s "${item}0" "/dev/dvb/adapter${card}/${item}${tuner}"
				done
			let "ind++,1"
			;;
		1)
			if data="$(sed -nr \
			-e '/Info:  DVB:   Frontend : ([[:alnum:]]+)/{s//\1/;p;q}' \
			-e '${q1}' <<< "${line}")"; then
				name="$(_trim "${data}")"
				if [ "${name}" = "Sony CXD2837ER DVB-T/T2/C demodulator" \
				-a ${tuner} -eq 1 ]; then
					Blacklist="${Blacklist:+"${Blacklist} "}${card}:0 "
					_applog "Info: Blacklisting this card ${card}:0" \
						"\"$(eval echo \"\${adapter${Adapters}_name}\")\""
				fi
				if grep -qswF "${card}:${tuner}" <<< "${Blacklist}"; then
					_applog "Info: Not using blacklisted ${dvbtype} card ${card}:${tuner} \"${name}\""
					ind=0
					continue
				fi
				eval adapter${adapter}_name='${name}'
				let "ind++,1"
			else
				ind=0
			fi
			;;
		2)
			if data="$(sed -nr \
			-e '/Info:  DVB:[[:blank:]]+([^[:blank:]].*)/{s//\1/;p;q}' \
			-e '${q1}' <<< "${line}")"; then
				case "$(_trim "${data}")" in
				Terrestrial*) dvbtype="dvbt" ;;
				Satellite*) dvbtype="dvbs" ;;
				Cable*) dvbtype="dvbc" ;;
				ATSC*) dvbtype="atsc" ;;
				*) ind=0; continue ;;
				esac
				eval adapter${adapter}_dvbtype='${dvbtype}'
				let "ind++,1"
			else
				ind=0
			fi
			;;
		3)
			if data="$(sed -nr \
			-e '/Info:  DVB:   Frequency: ([[:digit:]]+) kHz to ([[:digit:]]+) kHz/{s//\1 \2/;p;q}' \
			-e '${q1}' <<< "${line}")"; then
				eval adapter${adapter}_freqmin='${data%% *}'
				eval adapter${adapter}_freqmax='${data##* }'
				let "Adapters++,1"
				_applog "Info: Detected ${dvbtype} card ${card}:${tuner} \"${name}\""
				printf '%s' "${adapterTypes}" | \
				grep -qswF "${dvbtype}" || \
					adapterTypes="${adapterTypes}${dvbtype} "
			fi
			ind=0
			;;
		esac
	done < <(/usr/bin/mumudvb --list-cards 2>&1)

	for dvbtype in ${adapterTypes}; do
		while read -r conffile; do
			let "Configs++,1"
			eval config${Configs}_conffile='${conffile}'
			eval config${Configs}_dvbtype='${dvbtype}'

			name=""
			service_id=""
			while read -r line; do
				case "${line}" in
				freq=*) eval config${Configs}_freq='${line##*=}' ;;
				name=*) name="${line##*=}" ;;
				service_id=*) service_id="${line##*=}" ;;
				*)
					[ -n "${name}" -a -n "${service_id}" ] || \
						continue
					eval config${Configs}_service_${service_id}='${name}'
					name=""
					service_id=""
					;;
				esac
			done < "${conffile}"
			[ -z "${name}" -o -z "${service_id}" ] || \
				eval config${Configs}_service_${service_id}='${name}'
			RuleInsert $((PortHttp+Configs))
		done < <(ls -1 /etc/mumudvb/${dvbtype}_*.conf 2>> "${devNull}")
	done

	reloadConfig=""
	ListStatus &
}

ClientsDiff() {
	local clientsA="${1}" \
		clientsB="${2}"
	test -n "${clientsA}" && \
		grep -svxF "${clientsB:-0}" <<< "${clientsA}"
}

ServerName() {
	echo "mumudvb server ${dvbtype} ${portHttp}:${card}:${tuner}"
}

GetPlaylist() {
	# global: servername clientIP unit playlist log_file sleeping
	# returns: stuck
	# return-code: OK ERR
	local c=4 \
		msg
	: > "${playlist}"
	stuck=""
	while sleep 2;
	systemctl --quiet is-active "${unit}" && \
	[ ! -s "${playlist}" ]; do
		if ! wget --quiet -O "${playlist}" \
		"http://localhost:${portHttp}/playlist.m3u" || \
		[ $(wc -l < "${playlist}") -le 1 ]; then
			: > "${playlist}"
			! let "c--" || \
				continue
			[ -z "${Debug}" ] || \
				cp "${log_file}" "${log_file}_$(_UTCseconds)"
			if tail -n 1 "${log_file}" | \
			grep -qsF "Info:  Tune:  FE_STATUS:"; then
				stuck="can not tune adapter"
				return ${ERR}
			fi
			if [ -n "${stuck}" ]; then
				if grep -qsF "Unknown error 524" "${log_file}"; then
					stuck="adapter is stuck"
				fi
				return ${ERR}
			fi
			stuck="y"
			systemctl --quiet restart "${unit}" || \
				continue
			LogMsg "OnStartFail" "${clientIP}" \
				"Warn: ${servername} does not work. Restarting it"
			sleeping=${IdleTimeout}
			c=4
			sleep 2
		fi
	done
	systemctl --quiet is-active "${unit}" && \
	[ -s "${playlist}" ] || \
		return ${ERR}
	stuck=""
	LogMsg "OnAvail" "${clientIP}" "Info: ${servername} is available"
	if ! grep -qsF ":${portHttp}/bysid/" "playlist.m3u"; then
		[ -z "${Debug}" ] || \
			_applog "Info: adding playlist \"${playlist}\"" \
				"to \"playlist.m3u\""
		[ -s "playlist.m3u" ] && \
			tail -n +2 "${playlist}" >> "playlist.m3u" || \
			cp -f "${playlist}" "playlist.m3u"
	fi
}

WatchConnections() {
	# global: sleeping unit portHttp pipe fdPipeSrv clients
	# return-code: OK (allways)
	local line client connections msg msg1
	if connections="$(Traffic ${portHttp})"; then
		echo "CONNECTED" >&${fdPipeSrv}
	else
		sleeping=${IdleTimeout}
	fi
	while systemctl --quiet is-active "${unit}"; do
		read -t ${sleeping} -r -u "${fdPipeSrv}" line || {
			[ ${?} -ne ${RCALRM} ] || \
			[ ${sleeping} -ne ${IdleTimeout} ] || \
			[ -n "$(Traffic ${portHttp})" ] || \
				return ${OK}
			sleeping=${IdleTimeout}
			continue
		}
		[ "${line}" != "${ENDSERVER}" ] || \
			return ${OK}
		if [ "${line}" = "${ENDIDLESERVER}" ]; then
			Traffic ${portHttp} >> "${devNull}" || \
				return ${OK}
			LogMsg "Info: ${servername}," \
				"${LF}Clients:\"$(Hostnames ${connections})\""
			continue
		fi
		sleeping=${IdleTimeout}
		if connections="$(Traffic ${portHttp})"; then
			[ "${clients}" != "${connections}" ] || \
				continue
			sleeping=2
			msg="Info: ${servername}"
			msg1="${msg}"
			_applog "${msg}"
			if client="$(ClientsDiff "${clients}" "${connections}")"; then
				msg="Close:\"$(Hostnames ${client})\""
				msg1="${msg1},${LF}${msg}"
				_applog "${msg}"
			fi
			if client="$(ClientsDiff "${connections}" "${clients}")"; then
				msg="Open:\"$(Hostnames ${client})\""
				msg1="${msg1},${LF}${msg}"
				_applog "${msg}"
			fi
			msg="Clients:\"$(Hostnames ${connections})\""
			msg1="${msg1},${LF}${msg}"
			_applog "${msg}"
			[ -z "${ExitPoints}" ] || \
				((OnConnection "${msg1}" \
					$(ClientIPs ${connections} ${clients}) >> "${devNull}" 2>&1) &)
			clients="${connections}"
		elif [ -n "${clients}" ]; then
			LogMsg "OnConnection" "$(ClientIPs ${clients})" \
				"Info: ${servername} All closed:\"$(Hostnames ${clients})\""
			clients=""
		fi
	done
}

StartServer() {
	# global: config portHttp card tuner dvbtype clientIP
	local conffile tmpconf log_file playlist sleeping \
		pidSrvJournal pipe fdPipeSrv line pids \
		clients="" unit stuck servername msg \
		muxError=""

	servername="$(ServerName)"
	RuleClean ${portHttp}
	eval conffile=\"\${config${config}_conffile:-}\"
	tmpconf="${portHttp}-${card}-${tuner}-$(basename "${conffile}")"
	playlist="$(basename --suffix ".conf" "${conffile}").m3u"
	log_file="$(basename --suffix ".conf" "${tmpconf}").log"
	sed -re "/^port_http=.*/s//port_http=${portHttp}/" \
		-e "/^card=.*/s//card=${card}/" \
		-e "/^tuner=.*/s//tuner=${tuner}/" \
		< "${conffile}" > "${tmpconf}"

	: > "${log_file}"
	unit="${NAME}-${portHttp}.service"
	if systemctl list-unit-files --all "${unit}" > /dev/null 2>> "${devNull}"; then
		systemctl --quiet reset-failed "${unit}" 2>> "${devNull}" || :
	fi
	pipe="${portHttp}-pipe"
	rm -f "${pipe}"
	mkfifo "${pipe}"
	exec {fdPipeSrv}<> "${pipe}"
	chown _mumudvb:video "${log_file}" "${pipe}"
	chmod a+rw "${pipe}"
	if systemd-run --quiet \
	--working-directory="${Workdir}" \
	--unit="${unit}" \
	--uid=_mumudvb \
	--gid=video \
	--property="StandardOutput=append:${Workdir}${log_file}" \
	--property="StandardError=append:${Workdir}${log_file}" \
	--property="ExecStopPost=/bin/bash -c 'echo ${ENDSERVER}  >&${fdPipeSrv}'" \
	--property="KillMode=mixed" \
	--property="KillSignal=INT" \
	--property="TimeoutStopSec=10s" \
	/usr/bin/mumudvb ${Debug:+"-v"} -d -c "${tmpconf}"; then
		LogMsg "OnStart" "${clientIP}" "Info: starting ${servername}"
	fi
	sleep 2
	if systemctl --quiet is-active "${unit}"; then
		[ -z "${Debug}" ] || \
			_applog "Info: ${servername} is running"
		sleeping=2
		if GetPlaylist; then
			while read -t 0.1 -r -u ${fdPipeSrv} line; do
				:
			done
			set -o monitor
			( set +o errexit
			while :; do
				journalctl --since="@$(_UTCseconds)" \
				--follow "--grep=${CLIENTCONNECT}${portHttp}" --output=cat \
				>&${fdPipeSrv}
			done) &
			pidSrvJournal=${!}
			set +o monitor
			RuleInsertClient ${portHttp}
			WatchConnections
			RuleClean ${portHttp} "${CLIENTCONNECT}${portHttp}"
			kill -s TERM -- -${pidSrvJournal}
			_wait ${pidSrvJournal}
			exec {fdPipeSrv}<&-
			rm -f "${pipe}"
		else
			muxError="BlacklistMux ${portHttp}:${card}:${tuner} ${clientIP}"
		fi
		if systemctl --quiet is-active "${unit}"; then
			if [ "${stuck}" = "y" ]; then
				msg="Warn: ${servername} doesn't work"
			elif [ -n "${stuck}" ]; then
				msg="Warn: ${servername} ${stuck}"
			else
				msg="Info: ${servername} is idle"
			fi
			msg="${msg}. Stopping it"
			systemctl --quiet stop "${unit}" || :
		else
			msg="Info: ${servername} has stopped himself"
		fi
		LogMsg "OnStop" "$(ClientIPs ${clientIP} ${clients})" \
				"${msg}"
	else
		LogMsg "OnStartFail" "${clientIP}" "Err: ${servername} doesn't run"
	fi
	if systemctl list-unit-files --all "${unit}" > /dev/null 2>> "${devNull}"; then
		systemctl --quiet reset-failed "${unit}" 2>> "${devNull}" || :
	fi
	rm -f "${pipe}"
	echo "${ENDSERVER}${portHttp}" >&${fdPipe}
	[ -z "${muxError}" ] || \
		echo "${muxError}" >&${fdPipe}
}

SelectAdapter() {
	# global: config portHttp dvbtype clientIP
	# returns: card tuner
	# return-code: OK ERR
	local adapters freq freqmax freqmin adapter msg
	adapters="$(set | \
		sed -nre "/^adapter([[:digit:]]+)_dvbtype=${dvbtype}/s//\1/p")"
	eval freq=\"\${config${config}_freq:-}\"
	for adapter in ${adapters}; do
		eval card=\"\${adapter${adapter}_card:-}\"
		eval tuner=\"\${adapter${adapter}_tuner:-}\"
		if grep -qswF "${card}:${tuner}" <<< "${Blacklist}"; then
			[ -z "${Debug}" ] || \
				LogMsg "OnNoCardAvail" "${clientIP}" \
				"Info: Not selecting blacklisted card ${card}:${tuner}"
			continue
		fi
		if grep -qswF "${portHttp}:${card}:${tuner}" \
		<<< "${BlacklistMux}"; then
			LogMsg "OnNoCardAvail" "${clientIP}" \
				"Info: Not selecting blacklisted multiplexer ${portHttp}:${card}:${tuner}"
			continue
		fi
		if printf '%s' "${Servers}" | \
		awk -v card="${card}" -v tuner="${tuner}" \
		'BEGIN{RS="\t"; FS=":"}
		$2 == card && $3 == tuner {rc=-1; exit}
		END{exit rc+1}' || \
		_lsof "/dev/dvb/adapter${card}/demux${tuner}" >> "${devNull}"; then
			LogMsg "OnNoCardAvail" "${clientIP}" \
				"Info: ${portHttp} can't get adapter${card}/frontend${tuner}," \
				"it's already in use"
			continue
		fi
		eval freqmin=\"\${adapter${adapter}_freqmin:-}\"
		eval freqmax=\"\${adapter${adapter}_freqmax:-}\"
		[ ${freqmin} -gt ${freq} -o  ${freq} -gt ${freqmax} ] || {
			LogMsg "OnSelect" "${clientIP}" \
				"Info: ${portHttp} selects adapter${card}/frontend${tuner}"
			return ${OK}
		}
		LogMsg "OnNoCardAvail" "${clientIP}" \
			"Info: ${portHttp} frequency ${freq} out of range" \
			"adapter${card}/frontend${tuner}"
	done

	LogMsg "OnNoCardAvail" "${clientIP}" \
		"Warn: ${portHttp} no more ${dvbtype} cards available"
	return ${ERR}
}

_exit() {
	trap - EXIT INT
	set +o errexit +o nounset -o pipefail +o noglob
	if [ -n "${Servers}" ]; then
		for portHttp in $(printf '%s' "${Servers}" | \
		awk 'BEGIN{RS="\t"; FS=":"};{printf "%d\t", $1}'); do
			systemctl --quiet stop "${NAME}-${portHttp}.service" || :
		done
		_wait $(printf '%s' "${Servers}" | \
			awk 'BEGIN{RS="\t"; FS=":"}
			{printf "%d\t", $4}')
	fi
	RuleClean
	RuleClean "" "${CLIENTCONNECT}.*"
	pids="$(_ps_children)"
	[ -z "${pids}" ] || \
		kill -s TERM ${pids} 2>> "${devNull}" || :
	_wait
	_applog "Daemon exit"
}

Main() {
	# internal variables, daemon scope
	local fdPipe reloadConfig line portHttp config card tuner pid \
		dvbtype clientIP mux pidJournal clients msg msg1

	trap '_exit' EXIT
	trap 'exit' INT

	[ ! -s "/usr/lib/${NAME}/exit-points.sh" ] || {
		. "/usr/lib/${NAME}/exit-points.sh"
		ExitPoints="y"
	}

	ConfigLoad "${@}" || \
		exit ${ERR}

	trap '[ -n "${reloadConfig}" ] || echo "${RELOAD}" >&${fdPipe}' "${IRELOAD}"
	trap 'echo "ListStatus" >&${fdPipe}' "${ISTAT}"

	while :; do
		read -r -u "${fdPipe}" line || \
			continue
		if portHttp="$(sed -nr \
		-e "/.*${NEWREQUEST}.*DPT=([[:digit:]]+)[^[:digit:]]*.*/{s//\1/;p;q}" \
		-e '${q1}' <<< "${line}")"; then
			if printf '%s' "${Servers}" | \
			awk -v portHttp="${portHttp}" \
			'BEGIN{RS="\t"; FS=":"}
			$1 == portHttp {rc=-1; exit}
			END{exit rc+1}' || \
			Traffic ${portHttp} >> "${devNull}"; then
				[ -z "${Debug}" ] || \
					_applog "Warn: received new request" \
						"for already active port ${portHttp}"
				continue
			fi
			clientIP="$(sed -nr \
				-e "/.*SRC=([[:digit:].]+)[^[:digit:].]*.*/{s//\1/;p;q}" \
				<<< "${line}")"
			let "config=portHttp-PortHttp,1"
			eval dvbtype=\"\${config${config}_dvbtype:-}\"
			LogMsg "OnRequest" "${clientIP}" \
				"Info: $(Hostnames ${clientIP}) requests port ${portHttp}"
			SelectAdapter || \
				continue
			StartServer &
			Servers="${Servers}${portHttp}:${card}:${tuner}:${!}${TAB}"
			ListStatus &
		elif portHttp="$(sed -nr \
		-e "/^${ENDSERVER}([[:digit:]]+)$/{s//\1/;p;q}" \
		-e '${q1}' <<< "${line}")"; then
			_wait $(printf '%s' "${Servers}" | \
				awk -v portHttp="${portHttp}" \
				'BEGIN{RS="\t"; FS=":"}
				$1 == portHttp {printf "%d", $4; exit}')
			Servers="$(printf '%s' "${Servers}" | \
				awk -v portHttp="${portHttp}" \
				'BEGIN{RS="\t"; FS=":"}
				$1 != portHttp {printf "%s\t", $0}')"
			if [ -z "${Servers}" -a -n "${reloadConfig}" ]; then
				ConfigLoad "${@}" || \
					exit ${ERR}
				_applog "Info: configuration reloaded"
			else
				RuleInsert ${portHttp}
				ListStatus &
			fi
		elif [ "${line}" = "${RELOAD}" ]; then
			if [ -n "${Servers}" ]; then
				_applog "Warn: deferring configuration reload." \
					"Active servers: ${Servers}"
				reloadConfig="y"
			else
				ConfigLoad "${@}" || \
					exit ${ERR}
			fi
		elif [ "${line%% *}" = "BlacklistMux" ]; then
			mux="$(cut -f 2 -s -d ' ' <<< "${line}")"
			[ -z "${CanBlacklistMux}" ] || \
				BlacklistMux="${BlacklistMux}${mux}${TAB}"
			clientIP="$(cut -f 3 -s -d ' ' <<< "${line}")"
			LogMsg "OnStatus" "${clientIP}" \
				"Warn:" \
				"$(test -z "${CanBlacklistMux}" && \
				echo "Will not blacklist" || \
				echo "Blacklisting")" \
				"mux ${mux}"
			echo "${NEWREQUEST} DPT=$(cut -f 1 -s -d ':' <<< "${mux}") SRC=${clientIP}" >&${fdPipe}
			ListStatus &
		elif [ "${line%% *}" = "ListStatus" ]; then
			ListStatus &
			clientIP="${line##* }"
			if [ -z "${Servers}" ]; then
				LogMsg "OnStatus" "${clientIP}" "Status: no servers active"
				continue
			fi
			while IFS=":" read portHttp card tuner pid; do
				eval dvbtype=\"\${config$((portHttp-PortHttp))_dvbtype:-}\"
				msg="Status: $(ServerName) is active"
				msg1="${msg}"
				_applog "${msg}"
				if clients="$(Traffic ${portHttp})"; then
					msg="clients: $(Hostnames ${clients})"
					msg1="${msg1},${LF}${msg}"
				else
					msg="no clients connected"
					msg1="${msg1},${LF}${msg}"
				fi
				_applog "${msg}"
				[ -z "${ExitPoints}" ] || \
					((OnStatus "${msg1}" "${clientIP}" >> "${devNull}" 2>&1) &)
			done < <(printf '%s' "${Servers}" | \
				tr -s '\t' '\n')
		else
			[ -z "${Debug}" ] || \
				_applog "Warn: received invalid data from pipe" \
					"\"${line}\""
		fi
	done
}

# constants
NAME="$(basename "${0}")"
readonly NAME \
	STAT="status.txt" \
	PIPE="pipe" \
	LOG="log.txt" \
	IRELOAD="HUP" ISTAT="USR2" \
	OK=0 ERR=1 LF=$'\n' TAB=$'\t' NONE=0 \
	NEWREQUEST="${NAME}New:" \
	ENDSERVER="EndServer:" \
	ENDIDLESERVER="EndIdleServer:" \
	CLIENTCONNECT="CliConnect:" \
	RELOAD="reloadConfig" \
	RCALRM=$(_RcInt "ALRM")
# config variables
declare Workdir Debug PortHttp LogRotate IdleTimeout \
	Blacklist BlacklistMux CanBlacklistMux
ConfigDefault
# Main Global variables
declare Adapters Configs Servers="" ExitPoints="" devNull="/dev/null"

if [ "${1:-}" = "start" ]; then
	shift
	Main "${@}"
	exit 0
fi

if ! systemctl --quiet is-active dvbondemand.service; then
	echo "Err: Service not active" >&2
	exit ${ERR}
fi

case "${1:-}" in
status)
	# list status.txt
	d="$(_UTCseconds -r "${STAT}")"
	echo "Info: Updating status file" >&2
	if [ -z "${SSH_CLIENT:-}" ]; then
		clientIP="127.0.0.1"
	else
		clientIP="${SSH_CLIENT%% *}"
	fi
	exec {fdPipe}<> "${PIPE}"
	if echo "ListStatus ${clientIP}" >&${fdPipe} || \
	systemctl kill --signal "${ISTAT}" "${NAME}" 2> /dev/null; then
		c=5
		while sleep 1;
		[ $(_UTCseconds -r "${STAT}") -eq ${d} ] && \
		let "c--"; do
			:
		done
		[ $(_UTCseconds -r "${STAT}") -ne ${d} ] || \
			echo "Err: Error when updating status file" >&2
	fi
	cat "${STAT}"
	;;
endserver)
	# end mux server when idle
	shift
	config="${1}"
	portHttp=$((PortHttp+config))
	pipe="${portHttp}-pipe"
	if [ ! -p "${pipe}" ]; then
		echo "Err: Server not active in port ${portHttp}" >&2
		exit ${ERR}
	fi
	exec {fdPipeSrv}<> "${pipe}"
	echo "${ENDIDLESERVER}" >&${fdPipeSrv} || :
	echo "Info: Stopping idle server in port ${portHttp}" >&2
	;;
playlist)
	# get a playlist by config number 1..Configs
	shift
	config="${1}"
	conffile="$(awk -v conffile="config${config}_conffile" \
		'BEGIN{FS="="}
		$1 == conffile {print $2; rc=-1; exit}
		END{exit rc+1}' < "${STAT}")" && \
	[ -e "${conffile}" ] || \
		exit 1
	playlist="$(basename --suffix ".conf" "${conffile}").m3u"
	portHttp=$((PortHttp+config))
	c=4
	url="http://localhost:${portHttp}/playlist.m3u"
	while [ ! -s "${playlist}" ] && \
	let "c--"; do
		! wget --quiet -O /dev/null "${url}" || \
			break
		sleep 10
	done

	if [ -e "${portHttp}-pipe" ]; then
		sleep 5
		"${0}" endserver ${config} || :
		sleep 5
	fi

	if [ -s "${playlist}" ]; then
		echo "DVB services for MUX ${config}" \
			"available in http port ${portHttp}:" >&2
		echo "${playlist}"
		sed -re "\|http://127.0.0.1:|s||http://$(hostname):|" < "${playlist}"
	fi
	;;
allplaylists)
	# get all available playlists
	Configs="$(awk 'BEGIN{FS="="}
		$1 == "Configs" {print $2; exit}' < "${STAT}")"
	for config in $(seq ${Configs}); do
		"${0}" playlist ${config} || :
	done
	;;
*)
	echo "Err: Wrong arguments" >&2
	exit ${ERR}
	;;
esac
