#!/bin/bash

#  dvbondemand
#
#  Gets audio/video services from several dvb cards.
#  Starts mumudvb servers on client's request.
#  Stops these servers when idle.
#
#  dvbondemand detects when a service is required and therefore
#  starts mumudvb sessions using an available dvb adapter.
#  In few seconds clients will get the corresponding http video/audio stream.
#  Also, ends the mumudvb server session after client disconnection.
#
#  $Revision: 1.36 $
#
#  Copyright (C) 2023-2024 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

declare -r EP_users="jpujol@pcjordi live@pcjordi jpujol@gmktec"

EP_clients() {
	local ip="${1}" \
		addr user host
	for addr in ${EP_users}; do
		user="$(echo "${addr}" | cut -f 1 -s -d '@')"
		host="$(echo "${addr}" | cut -f 2 -s -d '@')"
		if [ -n "${host}" ] && \
		getent ahostsv4 "${host}" | grep -qsEe "^${ip}[[:blank:]]+"; then
			printf '%s@%s\t' "${user:-"dvbondemand"}" "${ip}"
		fi
	done
}

EP_Cmd() {
	local msg="${1}" \
		ip client users u d b localips localip
	shift
	localips="$(ip -4 addr list | \
		awk '$1 == "inet" {print substr($2, 0, index($2,"/")-1)}')"
	for ip in $(printf '%s\n' "${@}" | sort -u); do
		localip="$(grep -sxF "${ip}" <<< "${localips}")"
		for client in $(EP_clients "${ip}"); do
			users=""
			while IFS=" " read -r u d b; do
				[ -n "${u}" ] || \
					continue
				users="${users}${u}${TAB}"
				if [ -n "${localip}" ]; then
					((export "${d}" "${b}"
					sudo -u "${u}" --preserve-env=DISPLAY,DBUS_SESSION_BUS_ADDRESS -- \
					notify-send --app-name="${NAME}" -t 10000 "${msg}") &)
				else
					((ssh -f "${u}@${ip}" \
					-o ConnectTimeout=1 -o ConnectionAttempts=1 \
					"${d} ${b} \
					notify-send --app-name=${NAME} -t 10000 '${msg}'") &)
				fi
			done <<EOF
$(if [ -n "${localip}" ]; then
	ps axhe -o user,cmd | \
	sed -nre  "/^([^[:blank:]]+) .*( DBUS_SESSION_BUS_ADDRESS=[^[:blank:],]+) .*( DISPLAY=:[[:digit:]]+) .*/s//\1 \3 \2/p"
else
	ssh "${client}" \
	-o ConnectTimeout=1 -o ConnectionAttempts=1 \
	-o StrictHostKeyChecking=accept-new \
	ps axhe -o user,cmd | \
	sed -nre  "/^([^[:blank:]]+) .*( DBUS_SESSION_BUS_ADDRESS=[^[:blank:],]+) .*( DISPLAY=:[[:digit:]]+) .*/s//\1 \3 \2/p"
fi | \
sort -u)
EOF
# use this option only in trusted local networks
# -o StrictHostKeyChecking=accept-new \
			[ -z "${users}" ] || \
				break
		done
	done
}

OnRequest() {
	EP_Cmd "${@}"
}

OnSelect() {
	EP_Cmd "${@}"
}

OnStart() {
	EP_Cmd "${@}"
}

OnStartFail() {
	EP_Cmd "${@}"
}

OnAvail() {
	EP_Cmd "${@}"
}

OnStop() {
	EP_Cmd "${@}"
}

OnNoCardAvail() {
	EP_Cmd "${@}"
}

OnStatus() {
	EP_Cmd "${@}"
}

OnConnection() {
	EP_Cmd "${@}"
}

:
